execute pathogen#infect()
syntax on
filetype plugin indent on

"" Nerdtree
map <F3> :NERDTreeToggle<CR>

"" Color
colorscheme gruvbox

"" EasyMotion
let g:EasyMotion_smartcase = 1
let g:EasyMotion_startofline = 0
let g:EasyMotion_use_smartsign_us = 1
nmap <leader><leader>f <Plug>(easymotion-s2)
" <Leader>f{char} to move to {char}
map  <Leader>f <Plug>(easymotion-bd-f)
nmap <Leader>f <Plug>(easymotion-overwin-f)
map  <Leader>w <Plug>(easymotion-bd-w)
nmap <Leader>w <Plug>(easymotion-overwin-w)

"" Nerdcommenter
vmap ++ <plug>NERDCommenterToggle
nmap ++ <plug>NERDCommenterToggle

"" Teclas pessoais
map <localleader>o :bro ol<CR>
"map <leader>q :bro ol<CR>

if v:progname =~? "evim"
  finish
endif

set number
set guifont=Monaco:h10
set viminfo='10

set ch=1		" Make command line two lines high
set ignorecase
"set nobackup
"set noswapfile
"set nowritebackup
set backup
set dir=%TMP%
set backupdir=%TMP%
set directory=%TMP%
set noundofile

"" Encoding
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8
set bomb
set binary
set ttyfast


"" Tabs. May be overridden by autocmd rules
set tabstop=4
set softtabstop=0
set shiftwidth=4
set expandtab

set colorcolumn=80
set cursorline

"" Searching
set hlsearch
set incsearch
set ignorecase
set smartcase
"set inccommand=split

set fileformats=unix,dos,mac
" set viminfo='15

"colorscheme pablo


cnoreabbrev W! w!
cnoreabbrev Q! q!
cnoreabbrev Qall! qall!
cnoreabbrev Wq wq
cnoreabbrev Wa wa
cnoreabbrev wQ wq
cnoreabbrev WQ wq
cnoreabbrev W w
cnoreabbrev Q q
cnoreabbrev Qall qall


" Get the defaults that most users want.
source $VIMRUNTIME/defaults.vim

if &t_Co > 2 || has("gui_running")
  " Switch on highlighting the last used search pattern.
  set hlsearch
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78

  augroup END

else

  set autoindent		" always set autoindenting on

endif " has("autocmd")

" compatible.
if has('syntax') && has('eval')
  packadd matchit
endif

let mapleader='\'
let maplocalleader = '\\'

"" Copy/Past/Cut
set clipboard+=unnamedplus
set guioptions+=a
